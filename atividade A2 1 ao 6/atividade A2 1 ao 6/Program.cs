﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atividade_A2_1_ao_6
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                // 1. Uma empresa quer transmitir dados pelo telefone, mas está preocupada com a interceptação telefônica. Todos os seus dados são trasmitidos
                //como inteiros de quatro dígitos. Ela pediu para que você escreva um programa que criptografe seus dados, para que
                // eles possam ser transmitidos com mais segurança. Seu aplicativo deve ler um inteiro de quatro dígitos fornecidos pelo usuário
                // e criptografá-lo da seguinte forma: sebstitua cada dígito por (a soma desse dígito mais 7) módulo 10. Em seguida, troque o 
                // primeiro dígito pelo terceiro e troque o segundo dígito pelo quarto e imprima o inteiro criptografado

                // Declaração variáveis

                int Entrada = 0;
                int resto1 = 0;
                int resto2 = 0;
                int resto3 = 0;
                int temp1 = 0;
                int temp2 = 0;
                int temp3 = 0;
                int valor1 = 0;
                int valor2 = 0;
                int valor3 = 0;
                int valor4 = 0;
                int num1 = 0;
                int num2 = 0;
                int num3 = 0;
                int num4 = 0;

                Console.WriteLine("\tExercício1: \n");

                Console.WriteLine("Digite um número inteiro de quatro dígitos: \n");

                Entrada = int.Parse(Console.ReadLine());

                

                temp1 = (Entrada / 1000);
                resto1 = (Entrada % 1000);
                valor1 = temp1;

                temp2 = (resto1 / 100);
                resto2 = (resto1 % 100);
                valor2 = temp2;

                temp3 = (resto2 / 10);
                resto3 = (resto2 % 10);
                valor3 = temp3;

                valor4 = resto3;

                // Criptografando os dados:

                num1 = (valor1 + 7) % 10;
                num2 = (valor2 + 7) % 10;
                num3 = (valor3 + 7) % 10;
                num4 = (valor4 + 7) % 10;

                Console.Write("\n");

                //Imprimindo o número criptografado

                Console.WriteLine("O número digitado foi criptografado ao valor: \n");

                Console.Write(num3);
                Console.Write(num4);
                Console.Write(num1);
                Console.Write(num2);


                Console.ReadKey();
                Console.Clear();


                // 2. Crie um programa em C# que determina se o cliente de uma loja de departamentos ultrapassou o limite de crédito em uma conta.
                // Para cada cliente modelado em uma classe, os seguintes dados estão disponíveis:

                // a) Número da conta;
                // b) O saldo no início do mês;
                // c) O total de todos os itens cobrados desse cliente no mês corrente;
                // d) O total de todos os créditos aplicados na conta desse cliente no mês corrente;
                // e) O limite de crédito permitido.

                // O programa deve fornecer como valores inteiros cada um desses fatos, e através dos métodos da classe cliente: calcular o
                // novo saldo (= saldo inicial + cobranças - créditos), exibir o novo saldo e determinar se o novo saldo ultrapassa o limite de 
                // crédito do cliente. Para os clientes cujo o limite de crédito foi ultrapassado, o programa deve exibir a mensagem: " Limite
                // de crédito Excedido!". Teste a classe criada, bem como os métodos propostos na classe principal do seu programa.

                Console.WriteLine("Exercício 2:\n ");

                int saldo = 0;

                Console.WriteLine("Digite o valor do saldo:\n ");
                saldo = int.Parse(Console.ReadLine());

                Cliente cliente = new Cliente(saldo, 2, 300, 400, 2000);
                Console.WriteLine("O novo saldo é de:\n");
                Console.WriteLine(cliente.CalcularNovoSaldo());

                cliente.AtualizarSaldo();

                if (cliente.UltrapassouLimite())
                {
                    Console.WriteLine("Limite de Credito Excedido.");
                }
                else
                {
                    Console.WriteLine("Ainda há crédito disponivel.");
                }


                Console.ReadKey();
                Console.Clear();


                //3.Implemente em C# a função booleana ​Par ​que retorna verdadeiro se um número inteiro  
                // passado como parâmetro for par ou falso caso ele seja ímpar. Teste seu programa          
                //chamando a função para verificar os números de 0 à 10. 

                Console.WriteLine("Exercício 3:\n");

                int n = 0, r = 0;
                {
                    Console.WriteLine("Verifica se o número informado e par ou impar\n");
                    Console.Write("Informe um Número: ");
                    n = Convert.ToInt32(Console.ReadLine());
                    r = n % 2;
                    if (r == 0)
                    {
                        Console.WriteLine("o numero informado e par");

                    }
                    else
                    {
                        Console.WriteLine("o numero informado e impar");

                    }

                    Console.Write("Pressione Enter para continuar...\n");

                    Console.ReadKey();
                    Console.Clear();


                    // 4. Escreva uma programa que leia 3 números inteiros referente ao comprimento dos lados de um triângulo e classifique como:Triângulo equilátero,
                    //isósceles ou escaleno.

                    Console.WriteLine("\t Exercício 4: \n");

                    string lado1 = "";
                    string lado2 = "";
                    string lado3 = "";

                    Console.WriteLine("O Programa a seguir ira definir um determinado triângulo\nreferente as medidas dos seus lados.\n");

                    Console.WriteLine("Digite a medida de um dos lados do triângulo:\n");

                    lado1 = Console.ReadLine();

                    Console.WriteLine("\n");

                    Console.WriteLine("Digite a medida de outro lado do triângulo:\n");

                    lado2 = Console.ReadLine();

                    Console.WriteLine("\n");

                    Console.WriteLine("Digite a última medida do triângulo:\n");

                    lado3 = Console.ReadLine();

                    Console.WriteLine("\n");

                    if (lado1 == lado2 && lado2 == lado3)

                    {
                        Console.WriteLine("é um triângulo EQUILÁTERO");
                    }

                    else if (lado1 == lado2 || lado2 == lado3 || lado1 == lado3)

                    {
                        Console.WriteLine("é um triângulo ISÓSCELES");
                    }

                    else
                    {
                        Console.WriteLine("é um triângulo ESCALENO");
                    }

                    Console.ReadKey();
                    Console.Clear();

                    // 5. Escreva um programa que leia apenas uma letra do alfabeto como entrada e classifique-a como vogal ou consoante. Seu programa
                    // deverá aceitar como entrada apenas um caracter, ou seja, se o usuário digitar dois ou mais caracteres, o sistema deverá informar ao usuário 
                    // a entrada rejeitada.

                    Console.WriteLine("\t Exercício 5: \n");

                    int verifica = 0;

                    Console.WriteLine("Digite uma letra do alfabeto:\n");

                    var letra = Console.ReadLine().ToArray();

                    string[] vogais = new string[] { "a", "A", "e", "E", "i", "I", "o", "O", "u", "U" };


                    if (letra.Length == 1)
                    {
                        for (int i = 0; i < vogais.Length; i++)
                        {
                            if (letra[0].ToString() == vogais[i])
                            {
                                verifica = 1;
                            }
                        }

                        if (verifica == 1)
                        {
                            Console.WriteLine("\n \t A letra informada é uma VOGAL.");
                        }

                        else
                        {
                            Console.WriteLine("\n \t A letra informada é uma CONSOANTE ou um caracter qualquer.");
                        }

                    }
                    else
                    {
                        Console.WriteLine("ENTRADA RECUSADA!");
                    }

                    Console.ReadKey();
                    Console.Clear();

                    // 6. "Um número é Primo se ele for divisível por 1 e por ele mesmo". Escreva um programa que verifica se um número é primo.

                    Console.WriteLine("\t Exercício 6: \n");

                    Console.WriteLine("Digite um número maior que 2 e verifique se ele é Primo ou não:\n");

                    int primo = 0;
                    int verificador = 0;
                    int restodivisao = 0;
                    int denominador = 0;


                    primo = int.Parse(Console.ReadLine());

                    for (denominador = 2; denominador < primo; denominador++)
                    {
                        restodivisao = primo % denominador;

                        if (restodivisao == 0)
                        {
                            verificador = 1;
                        }
                    }

                    if (verificador == 1)
                    {
                        Console.WriteLine("O número digitado não é Primo.");
                    }
                    else
                    {
                        Console.WriteLine("O número digitado é Primo.");
                    }
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }
    }
}

